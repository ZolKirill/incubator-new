export class UpdateObjectDto {
  readonly id: number;
  readonly title: string;
  readonly isCompleted: boolean;
}
