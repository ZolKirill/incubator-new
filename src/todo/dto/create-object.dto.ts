export class CreateObjectDto {
  readonly id: number;
  readonly title: string;
  readonly isCompleted: boolean;
}
