import {
  Body,
  Delete,
  Param,
  Controller,
  Get,
  Post,
  Put,
} from '@nestjs/common';
import { CreateObjectDto } from './dto/create-object.dto';
import { UpdateObjectDto } from './dto/update-object.dto';
import { TodoService } from './todo.service';
import { Todo } from '../entities/todo.entity';

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  getAll(): Promise<Todo[]> {
    return this.todoService.getAll();
  }

  @Get(':id')
  getId(@Param('id') id: number): string {
    return this.todoService.getId(id);
  }
  @Post('create')
  create(@Body() todo: CreateObjectDto): CreateObjectDto {
    return this.todoService.create(todo);
  }
  @Put(':id')
  update(@Body() todo: UpdateObjectDto, @Param('id') id: number) {
    return this.todoService.update(todo, id);
  }
  @Delete(':id')
  delete(@Param('id') id: number): string {
    return this.todoService.delete(id);
  }
}
