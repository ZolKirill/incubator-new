import { Injectable } from '@nestjs/common';
import { CreateObjectDto } from './dto/create-object.dto';
import { UpdateObjectDto } from './dto/update-object.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from '../entities/todo.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo) private readonly todoRepository: Repository<Todo>,
  ) {}
  getAll() {
    return this.todoRepository.find();
  }
  getId(id): string {
    return `User id: ${id}`;
  }
  create(todo: CreateObjectDto): CreateObjectDto {
    console.log(todo);
    return todo;
  }
  update(todo: UpdateObjectDto, id: number): string {
    return `Updated post with id: ${id}`;
  }
  delete(id: number): string {
    return `Deleted user id: ${id}`;
  }
}
